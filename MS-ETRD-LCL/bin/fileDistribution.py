import pysftp
import json
import sys
import argparse
import logging
import time
import os
import math
import re
import zipfile
import gzip
import shutil
import datetime


ordinal = lambda n: "%d%s" % (n,"tsnrhtdd"[(math.floor(n/10)%10!=1)*(n%10<4)*n%10::4])

yyyymmdd = time.strftime('%Y%m%d')
hhmmdd = time.strftime('%H%M%S')
pid = os.getpid()

if not os.path.exists('/home/acdba/local/logs/'+yyyymmdd):
    os.makedirs('/home/acdba/local/logs/'+yyyymmdd)


#logging.basicConfig(filename='/home/acdba/local/logs/'+yyyymmdd+'/fileDistribution.'+hhmmdd+'.'+str(pid)+'.log', level=logging.INFO,format='%(asctime)s %(levelname)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

def log(level, message):
   if level == 'info':
      logging.info(message)
      print(level + ':' + message)
   elif level == 'warning':
      logging.warning(message)
      print(level + ':' + message)
   elif level == 'debug':
      logging.debug(message)
      print(level + ':' + message)

def date_conversion(dateFormat):
   if 'YYYYMMDD' in dateFormat:
      return(dateFormat.replace('YYYYMMDD',args.date))  
   elif 'MMDDYYYY_' in dateFormat:
      return(dateFormat.replace('MMDDYYYY',MMDDYYYY))
   elif 'MMDDYY-' in dateFormat:
      return(dateFormat.replace('MMDDYY',MMDDYY))
   elif 'YYYY-MM-DD' in dateFormat:
      return(dateFormat.replace('YYYY-MM-DD',YYYY_MM_DD))
   else:
      return(dateFormat)

parser = argparse.ArgumentParser()
parser.add_argument('-v', '--vendor')
parser.add_argument('-dir', '--directory')
parser.add_argument('-d', '--date')
parser.add_argument('-r', '--retry', default=1)
args = parser.parse_args()

logging.basicConfig(filename='/home/acdba/local/logs/'+yyyymmdd+'/fileDistribution.'+args.directory+'.'+hhmmdd+'.'+str(pid)+'.log', level=logging.INFO,format='%(asctime)s %(levelname)s:%(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')


MMDDYY = str(args.date)[4:6]+''+str(args.date)[6:8]+''+str(args.date)[0:2]
MMDDYYYY = str(args.date)[4:6]+''+str(args.date)[6:8]+''+str(args.date)[0:4]
YYYY_MM_DD = str(args.date)[0:4]+'-'+str(args.date)[4:6]+'-'+str(args.date)[6:8]

retryFlag = False

def main():
   global retryFlag
   t = time.localtime()
   current_time = time.strftime("%H%M%S", t)
   numDir = 0
   remoteMultifileCheck = False
   localMultifileCheck = False
   remotefileCheck = False
   localfileCheck = False
   multiFile = []
   json_file =  open('/home/acdba/local/conf/fileDistribution.json')
   data = json.load(json_file)
   start = ""
   end = ""
   duration = ""
   checkNumFiles = 0
   totalNumFilesDownload = 0
   for vendor in data['vendors']:
      if args.vendor in vendor:
         hostname = vendor[args.vendor][0]['hostname']
         username = vendor[args.vendor][0]['username']
         password = vendor[args.vendor][0]['password']
         protocol = vendor[args.vendor][0]['protocol']
         if protocol == 'sftp':
            #sftp = pysftp.Connection(hostname, username=username, password=password)
            sftp = pysftp.Connection(hostname, username=username)
            sftp.timeout = 60
         for _directory in vendor[args.vendor][0]['directories']:
            numDir = numDir + 1
            if args.directory in _directory:
               for _file in _directory[args.directory]:
                  checkNumFiles = checkNumFiles + 1
                  numFiles = len(_directory[args.directory])
                  remoteFilename = date_conversion(str(_file['remote_name']))
                  localFilename = date_conversion(str(_file['local_name']))
                  remotePath = date_conversion(str(_file['remote_path']))
                  localPath = date_conversion(str(_file['local_path']))
                  category = _file['category']
                  if '*' in remoteFilename:
                     filelist = sftp.listdir(remotePath)
                     for filename in filelist:
                        filedata = re.search(remoteFilename, filename)
                        if filedata:
                           multiFile.append(filename)
                           remotefileCheck = True
                           remoteMultifileCheck = True
                       
                  else:
                     remotefileCheck = sftp.exists(remotePath+'/'+remoteFilename)
                  localfileCheck = os.path.exists(localPath+'/'+localFilename)
                  if not os.path.exists(localPath):
                     os.makedirs(localPath)
                  os.chdir(localPath)
                  os.chdir("../")
                  if localfileCheck == False:
                     if remotefileCheck == True:
                        if remoteMultifileCheck == True:
                           for eachFile in multiFile:
                              localfileCheck = os.path.exists(localPath+'/'+eachFile)
                              if localfileCheck == False:
                                 localfileCheck = os.path.exists(localPath+'/processed/'+eachFile) 
                              if localfileCheck == False:
                                 log('info', 'File '+remotePath+'/'+eachFile+' exists, downloading the file to '+os.getcwd())
                                 start =  datetime.datetime.now()
                                 sftp.get(remotePath+'/'+eachFile)
                                 end =  datetime.datetime.now()
                                 duration = end - start
                                 log('info', 'File '+remotePath+'/'+eachFile+' took ' + str(duration) +' to download')
                                 totalNumFilesDownload = totalNumFilesDownload + 1
                                 if category == 'NERDS':
                                    log('info', 'Renaming the remote file to '+remotePath+'/'+eachFile+'.'+str(current_time)+'.downloaded')
                                    #sftp.remove(remotePath+'/'+eachFile)
                                    sftp.rename(remotePath+'/'+eachFile, remotePath+'/'+eachFile+'.'+str(current_time)+'.downloaded')
                                 if zipfile.is_zipfile(os.getcwd()+'/'+eachFile):
                                    log('info',eachFile + ' is compressed, uncompressing to '+localPath)
                                    with zipfile.ZipFile(os.getcwd()+'/'+eachFile) as zip_ref:
                                       zip_ref.extractall(localPath)
                                    log('info', 'Moving '+eachFile+' to '+localPath+'/'+eachFile)
                                    if category == 'NERDS':
                                       fs = eachFile.split('.')
                                       os.rename(eachFile,localPath+'/'+fs[0]+'_'+current_time+'.'+fs[1])
                                       #os.rename(eachFile,localPath+'/'+eachFile+'_'+current_time)
                                    else:
                                       os.rename(eachFile,localPath+'/'+eachFile)
                                 elif eachFile.split(".")[1] == 'gz':
                                    try:
                                       with gzip.open(os.getcwd()+'/'+eachFile, 'rb') as f_out:
                                          with open(localPath+'/'+localFilename, 'wb') as f_in:
                                             log('info','Decompressing '+os.getcwd()+'/'+eachFile+' to '+localPath+'/'+localFilename)
                                             shutil.copyfileobj(f_out, f_in)
                                             log('info', 'Moving '+eachFile+' to '+localPath+'/'+eachFile)
                                             if category == 'NERDS':
                                                fs = eachFile.split('.')
                                                os.rename(eachFile,localPath+'/'+fs[0]+'_'+current_time+'.'+fs[1])
                                                #os.rename(eachFile,localPath+'/'+eachFile+'_'+current_time)
                                             #else:
                                             #   os.rename(eachFile,localPath+'/'+eachFile+'.gz')
                                    except:
                                       log('fatal','Unable to decompress '+os.getcwd()+'/'+eachFile)     
                                       strException = 'Unable to decompress '+os.getcwd()+'/'+eachFile+', file might be corrupted'
                                       raise Exception(strException)
                                 else:
                                    log('info',eachFile + ' is not compressed, nothing to do here')
                                    log('info', 'Moving '+eachFile+' to '+localPath+'/'+eachFile+'.gz')
                                    if category == 'NERDS':
                                       fs = eachFile.split('.')
                                       os.rename(eachFile,localPath+'/'+fs[0]+'_'+current_time+'.'+fs[1])
                                       #os.rename(eachFile,localPath+'/'+eachFile+'_'+current_time)
                                    else:
                                       os.rename(eachFile,localPath+'/'+eachFile)
                              else:
                                 log('info', 'File '+localPath+'/'+eachFile+' already exists in local directory, skip downloading')

                              
                        else:   
                           log('info', 'File '+remotePath+'/'+remoteFilename+' exists, downloading the file to '+os.getcwd())
                           start =  datetime.datetime.now()
                           sftp.get(remotePath+'/'+remoteFilename)
                           end =  datetime.datetime.now()
                           duration = end - start
                           log('info', 'File '+remotePath+'/'+remoteFilename+' took ' + str(duration) +' to download')
                           totalNumFilesDownload = totalNumFilesDownload + 1
                           if category == 'NERDS':
                              #sftp.remove(remotePath+'/'+remoteFilename)
                              log('info', 'Renaming the remote file to '+remotePath+'/'+remoteFilename+'.'+str(current_time)+'.downloaded')
                              sftp.rename(remotePath+'/'+remoteFilename, remotePath+'/'+remoteFilename+'.'+str(current_time)+'.downloaded')
                           if zipfile.is_zipfile(os.getcwd()+'/'+remoteFilename):
                              log('info',remoteFilename + ' is compressed, uncompressing to '+localPath)
                              with zipfile.ZipFile(os.getcwd()+'/'+remoteFilename) as zip_ref:
                                 zip_ref.extractall(localPath)
                              log('info', 'Moving '+remoteFilename+' to '+localPath+'/'+localFilename)
                              if category == 'NERDS':
                                 fs = localFilename.split('.')
                                 os.rename(remoteFilename,localPath+'/'+fs[0]+'_'+current_time+'.'+fs[1])
                                 #os.rename(remoteFilename,localPath+'/'+localFilename+'_'+current_time)
                              else:
                                 os.rename(remoteFilename,localPath+'/'+localFilename)
                           elif remoteFilename.split(".")[1] == 'gz':
                              try:
                                 with gzip.open(os.getcwd()+'/'+remoteFilename, 'rb') as f_out:
                                    with open(localPath+'/'+localFilename, 'wb') as f_in:
                                       log('info','Decompressing2 '+os.getcwd()+'/'+remoteFilename+' to '+localPath+'/'+localFilename)
                                       shutil.copyfileobj(f_out, f_in)
                                       log('info', 'Moving '+remoteFilename+' to '+localPath+'/'+localFilename+'.gz')
                                       if category == 'NERDS':
                                          fs = localFilename.split('.')
                                          os.rename(remoteFilename,localPath+'/'+fs[0]+'_'+current_time+'.'+fs[1])
                              except:         
                                 log('fatal','Unable to decompress '+os.getcwd()+'/'+remoteFilename)        
                                 strException = 'Unable to decompress '+os.getcwd()+'/'+eachFile+', file might be corrupted'
                                 raise Exception(strException)
                           else:
                              log('info',remoteFilename + ' is not compressed, nothing to do here')
                              log('info', 'Moving '+remoteFilename+' to '+localPath+'/'+localFilename)
                              if category == 'NERDS':
                                 fs = localFilename.rsplit('.', 1)
                                 os.rename(remoteFilename,localPath+'/'+fs[0]+'_'+current_time+'.'+fs[1])
                                 #os.rename(remoteFilename,localPath+'/'+localFilename+'_'+current_time)
                              else:
                                 os.rename(remoteFilename,localPath+'/'+localFilename)

                     else:
                        retryFlag = True
                        #checkNumFiles = checkNumFiles + 1
                        log('warning', 'File '+remotePath+'/'+remoteFilename+' doesnt exists, will try later')
                        print(numFiles, checkNumFiles)
                        #if checkNumFiles == numFiles:
                        #   strException = 'File '+remotePath+'/'+remoteFilename+' doesnt exists, will try later'
                        #   raise Exception(strException)
                  else:
                     log('info', 'File '+localPath+'/'+localFilename+' already exists in local directory, skip downloading')
               break
            else:
               if numDir == len(vendor[args.vendor][0]['directories']):
                  log('warning', 'There is no '+args.directory+' directory in the json file')
   if retryFlag == True:
      log('fatal', 'Downloaded '+str(totalNumFilesDownload)+' out of '+str(numFiles))
      raise Exception('Downloaded '+str(totalNumFilesDownload)+' out of '+str(numFiles)) 
   sftp.close()
main()
for retry in range(int(args.retry)):
   retryAttempt = retry + 2
   if int(args.retry) > 1 and retry+1 < int(args.retry) and retryFlag == True:
      log('info', 'Sleeping for 5 minutes...')
      time.sleep(300)
      log('info', 'Downloading the files again - '+str(ordinal(retryAttempt))+ ' attempt')
      main()
